# A PHP SMS gateway server for use with EnvayaSMS #

**[Documentation](https://gitlab.com/sindrel/sms-server/wikis/home)**

This enables you to set up your own (free) gateway using an Android GSM device (phone, tablet) and a web server.

It's designed to be straight-forward, lightweight and simple.


Current functionality includes:
- Storing incoming messages
- Retrieving stored messages
- Forwarding received text messages to web application/servers using HTTP POST or HTTP GET requests (JSON and XML formatting supported)
- Forwarding incoming messages to your Slack team
- Sending outgoing messages from external applications
- Contacts


Configuration is done using .json files, and actions can be triggered based on keywords or phone numbers. For example, you can have all incoming messages with the keyword "SLACK" forwarded to your favorite Slack team/channel. You can also have all text messages from a specific number sent to another Web API using HTTP requests. And you can have all messages from your annoying cousin Bob return a message for him to stop spamming your new SMS server.


It can also be used as a tool for monitoring larger production SMS gateways, and can be integrated with Sensu or Nagios plugins for end-to-end testing.


If you want SMS notifications from your monitoring tools, simply use HTTP GET requests to send messages from applications such as Solarwinds Orion, Nagios or LibreNMS.


All you need is:
* An Android compatible device with GSM (a phone)
* A SIM card with a valid subscription
* A (stable) WiFi/3G/4G internet connection
* The EnvayaSMS app (available from the Google Play Store)
* A web server supporting php and php-curl



This server, and the Android app, is based on an open-source project called EnvayaSMS (https://github.com/youngj/EnvayaSMS). For more information and documentation on EnvayaSMS, visit http://sms.envaya.org/.

# Documentation #

**For installation instructions and documentation, read the [wiki](https://gitlab.com/sindrel/sms-server/wikis/home).**

# Screenshots #

Text message sent to SMS server:
![alt text](../screenshots/sms_send.png "Text message sent to SMS server")

Text message received in Slack, from contact 'Albert':
![alt text](../screenshots/slack.png "Text message received in Slack")

Text message received from SMS server, sent from monitoring system:
![alt text](../screenshots/sms_recv.png "Text message received from SMS server")