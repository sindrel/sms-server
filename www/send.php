<?php

/*
 * Sends SMS using a simple GET request
 * Example URL:
 * https://<baseurl>/send.php?user=<username>&password=<password>&to=<recipient>&message=<message>
 * sindrelindstad@gmail.com
*/

require_once "config.php";
require_once $SERVERPATH."EnvayaSMS.php";

try {
	$user = $_GET['user'];
	$password = $_GET['password'];
	$to = $_GET['to'];
	$body = $_GET['message'];

	// If user authentication is OK
	if($user == $SMS_USER && $password == $SMS_PASS) {
		if($to != "" && $body != "") {
			$message = new EnvayaSMS_OutgoingMessage();
			$message->id = uniqid("");
			$message->to = $to;
			$message->message = $body;

			file_put_contents("$OUTGOING_DIR_NAME/{$message->id}.json", json_encode($message));
			http_response_code(200); // OK

			echo "OK";
		} else {
			http_response_code(500); // Exception
		}
	} else {
		http_response_code(403); // Unauthorized
	}
}
catch(Exception $e) {
  //echo 'Message: ' .$e->getMessage();
  http_response_code(500);
}
