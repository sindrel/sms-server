<?php

/*
 * Outputs unread messages
 * Example URL:
 * https://<baseurl>/get.php?user=<username>&password=<password>&from=all&keyword=all&id=all&remove=0
 * sindrelindstad@gmail.com 
*/

require_once "config.php";
require_once $SERVERPATH."EnvayaSMS.php";

try {
	$user = $_GET['user'];
	$password = $_GET['password'];
	$from = $_GET['from'];
	$keyword = strtoupper($_GET['keyword']);
	$id = $_GET['id'];
	$remove = $_GET['remove'];

	// If user authentication is OK
	if($user == $SMS_USER && $password == $SMS_PASS) {
		if($from != "" && $keyword != "" && $id != "" && $remove != "") {
			http_response_code(200); // OK
			$files = array_diff(scandir($INCOMING_DIR_NAME), array('.', '..', '.gitignore'));
			arsort($files);

			$messageList = array();

			foreach($files as $file) {
				$path = $INCOMING_DIR_NAME."/$file";
				$content = json_decode(file_get_contents($path));
				$filterPassed = true;

				if($id != "all" && $id != $content->id) {
					$filterPassed = false;
				}

				if($from != "all" && $from != $content->from) {
					$filterPassed = false;
				}

				if($keyword != "ALL" && $keyword != $content->keyword) {
					$filterPassed = false;
				}

				if($filterPassed == true) {
					array_push($messageList, $content);

					// If specified, delete message(s) after
					if($remove == "1") {
						unlink($path);
					}
				}				
			}

			echo json_encode($messageList);

		} else {
			http_response_code(500); // Exception
		}
	} else {
		http_response_code(403); // Unauthorized
	}
}
catch(Exception $e) {
  //echo 'Message: ' .$e->getMessage();
  http_response_code(500);
}

?>