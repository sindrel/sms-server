<?php

/*
 * Handles incoming SMS messages
 * sindrelindstad@gmail.com 
*/

require_once "config.php";
require_once $SERVERPATH."EnvayaSMS.php";

function HandleIncomingMessage($CONFIG_DIR_NAME, $CONTACTS_DIR_NAME, $incomingFolder, $action) {
	$messageReply = "";

	try {
		$messageReceived = explode(" ", $action->message, 2);
		$epoch = $seconds = $action->timestamp / 1000;

		$dt = new DateTime("@$epoch");
		$messageData["datetime"] = $dt->format('Y-m-d H:i:s');
		$messageData["keyword"] = strtoupper($messageReceived[0]);
 
		if(array_key_exists(1, $messageReceived)) {
			$messageData["text"] = $messageReceived[1];
		}

		$contact = GetContact($CONTACTS_DIR_NAME, $action->from);

		if($contact) {
			$contactName = $contact->name;
		} else {
			$contactName = trim($action->from);
		}

		BeforeGetConfiguration:
		$configuration = GetConfiguration($CONFIG_DIR_NAME, $messageData["keyword"], $contactName);

		// If action rule matched on contact, use full message text including keyword
		if($configuration->matched == "contact") {
			$messageData["text"] = $action->message;
		}

		if($configuration) {
			// Slack
			if(strtolower($configuration->action) == "slack") {
				PostToSlack($configuration->config->webhook_url, $messageData["keyword"], $messageData["datetime"], $contactName, $messageData["text"], $configuration->config->channel, $configuration->config->name, $configuration->config->icon, $configuration->config->color, $configuration->config->link, $configuration->config->image_url);
			}
			// Store message
			elseif(strtolower($configuration->action) == "save") {
				StoreIncomingMessage($incomingFolder, $messageData["keyword"], $messageData["datetime"], $contactName, $messageData["text"]);
			}
			// HTTP post
			elseif(strtolower($configuration->action) == "http_post") {
				PostHTTP($configuration, $messageData, $contactName);
			}
			// HTTP get
			elseif(strtolower($configuration->action) == "http_get") {
				GetHTTP($configuration, $messageData, $contactName);
			}
			// If reply
			if($configuration->reply) {
				$messageReply = $configuration->reply_msg;
			}
		// If no configuration found
		} elseif(!$configuration && $messageData["keyword"] != "_UNKNOWN") {
			$messageData["text"] = $messageData["keyword"]." ".$messageData["text"];
			$messageData["keyword"] = "_UNKNOWN";
			goto BeforeGetConfiguration; // Yup...
		}
	}
	catch(Exception $e) {
	  echo 'Message: ' .$e->getMessage();
	}

	return $messageReply;
}

function GetConfiguration($CONFIG_DIR_NAME, $requestedKeyword, $contactName) {
	try {
		// Load all .json files in config folder
		$configFiles = array_diff(scandir($CONFIG_DIR_NAME), array('.', '..', '.gitignore', 'examples'));
		arsort($configFiles);

		foreach($configFiles as $file) {
			$content = file_get_contents("$CONFIG_DIR_NAME/$file");
			$contentArray = json_decode($content);
			
			foreach($contentArray as $keyword => $parameters){
				if(strtoupper($keyword) == $requestedKeyword) {
					$parameters->matched = "keyword";
					return $parameters; // Return parameters if keyword match
				} elseif(str_replace("_", "", $keyword) == $contactName) {
					$parameters->matched = "contact";
					return $parameters; // Return parameters if keyword matches number or contact
				}
			}
		}
		return; // Nothing found
	}
	catch(Exception $e) {
	  //echo 'Message: ' .$e->getMessage();
	  http_response_code(500);
	}
}

function GetContact($CONTACTS_DIR_NAME, $requestedFrom) {
	$requestedFrom = trim($requestedFrom);

	try {
		// Load all .json files in contacts folder
		$contactFiles = array_diff(scandir($CONTACTS_DIR_NAME), array('.', '..', '.gitignore', 'examples'));
		arsort($contactFiles);

		foreach($contactFiles as $file) {
			$content = file_get_contents("$CONTACTS_DIR_NAME/$file");
			$contentArray = json_decode($content);
			
			foreach($contentArray as $from => $parameters){
				if(trim($from) == $requestedFrom) {
					return $parameters; // Return parameters if keyword match
				}
			}
		}
		return;
	}
	catch(Exception $e) {
	  //echo 'Message: ' .$e->getMessage();
	  http_response_code(500);
	}
}

function PostHTTP($configuration, $messageData, $contactName) {

	// JSON
	if($configuration->config->content_type == "application/json") {
		$payload = array();
		if($configuration->config->payload_from != "") {
			$payload[$configuration->config->payload_from] = $contactName;
		}
		if($configuration->config->payload_time != "") {
			$payload[$configuration->config->payload_time] = $messageData["datetime"];
		}
		if($configuration->config->payload_text != "") {
			$payload[$configuration->config->payload_text] = $messageData["text"];
		}

		$payload = json_encode($payload);

	// XML
	} elseif ($configuration->config->content_type == "application/xml") {
		$payload = array();
		if($configuration->config->payload_from != "") {
			$payload[$contactName] = $configuration->config->payload_from;
		}
		if($configuration->config->payload_time != "") {
			$payload[$messageData["datetime"]] = $configuration->config->payload_time;
		}
		if($configuration->config->payload_text != "") {
			$payload[$messageData["text"]] = $configuration->config->payload_text;
		}

		$xml = new SimpleXMLElement('<data/>');
		array_walk_recursive($payload, array ($xml, 'addChild'));
		$payload = (string)$xml->asXML();
	}

	// Post
	try {                                                                                                         
		$curl = curl_init($configuration->config->url);   
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($curl, CURLOPT_POSTFIELDS, $payload);                                                                  
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_FAILONERROR, FALSE);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array(                                                                                         
		    'Content-Type: '.$type,                                                                   
		    'Content-Length: ' . strlen($payload))                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($curl);
		curl_close($curl);
	}
	catch(Exception $e) {
	  echo 'Message: ' .$e->getMessage();
	}
}

function GetHTTP($configuration, $messageData, $contactName) {

	// If existing parameters in URL
	if (strpos($configuration->config->url, '?') !== false) {
    	$firstParamChar = "&";
	} else {
		$firstParamChar = "?";
	}

	$url = $configuration->config->url.$firstParamChar."datetime=".urlencode($messageData["datetime"])."&from=".urlencode($contactName)."&keyword=".urlencode($messageData["keyword"])."&message=".urlencode($messageData["text"]);

	try {
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($curl, CURLOPT_FAILONERROR, FALSE);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

		$result = curl_exec($curl);
		curl_close($curl);

		return $result;
	}
	catch(Exception $e) {
	  echo 'Message: ' .$e->getMessage();
	}
}

function StoreIncomingMessage($folder, $keyword, $datetime, $from, $message) {
	$id = md5(uniqid(rand(), true));

	$output = array(
		"id" => $id,
		"datetime" => $datetime,
		"keyword" => $keyword,
		"from" => $from,
		"message" => $message
		);

	$output = json_encode($output);
	file_put_contents($folder."/$id.json", $output);
}

function PostToSlack($url, $keyword, $datetime, $from, $message, $channel = "#general", $username = "Text message", $icon = ":iphone:", $color = "good", $titleLink = "", $imageUrl = "") {
    $attachments = array(
    		"fallback"      =>  "SMS from $from: $message (received: $datetime)",
    		//"pretext"       =>  "From: $from",
    		"title"       	=>  "$datetime\nFrom: $from",
    		"text"       	=>  $message,
    		"color"       	=>  $color
    		);

    if($titleLink != "") {
    	$attachments["title_link"] = $titleLink;
    }
    if($imageUrl != "") {
    	$attachments["image_url"] = $imageUrl;
    }

    $data = "payload=" . json_encode(array(
            "channel"       =>  $channel,
            "username"      =>  $username,
            //"text"          =>  $message,
            "icon_emoji"    =>  $icon,
            "attachments"   =>  [$attachments]
            ));
	
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($ch);
    curl_close($ch);

    echo $data;

    return $result;
}


?>
