<?php
ini_set('display_errors','0');

/* REQUIRED CONFIGURATION  START */

// This is the path to where your backend folder is located
$SERVERPATH = '/srv/sms-server/';

// EnvayaSMS password (must match the password set in the app on your Android device)
$PASSWORD = 'rosebud';

// Username and password for sending outgoing SMS
$SMS_USER = 'sms';
$SMS_PASS = 'peanuts';

/*	REQUIRED CONFIGURATION  END */



/*
 * example/send_sms.php uses the local file system to queue outgoing messages
 * in this directory.
 */

$OUTGOING_DIR_NAME = $SERVERPATH."outgoing_sms";
$INCOMING_DIR_NAME = $SERVERPATH."incoming_sms";
$CONFIG_DIR_NAME = $SERVERPATH."actions";
$CONTACTS_DIR_NAME = $SERVERPATH."contacts";

/*
 * AMQP allows you to send outgoing messages in real-time (i.e. 'push' instead of polling).
 * In order to use AMQP, you would need to install an AMQP server such as RabbitMQ, and 
 * also enter the AMQP connection settings in the app. (The settings in the EnvayaSMS app
 * should use the same vhost and queue name, but may use a different host/port/user/password.)
 */

$AMQP_SETTINGS = array(
    'host' => 'localhost',
    'port' => 5672,
    'user' => 'guest',
    'password' => 'guest',
    'vhost' => '/',
    'queue_name' => "envayasms"
);

